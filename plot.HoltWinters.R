#####
## Grafico Holt Winters
#
plot.HoltWinters <-function(hw_object, n.ahead = 12, CI=.95, error.ribbon = 'green', line.size=1){

  forecast <- predict(object = hw_object, 
                      n.ahead = n.ahead, 
                      prediction.interval = TRUE, 
                      level = CI)

  for_values <- data.frame(time = round(x = time(forecast), digits = 3),
                           value_forecast = as.data.frame(forecast)$fit,  
                           dev = as.data.frame(forecast)$upr - as.data.frame(forecast)$fit)
  
  fitted_values <- data.frame(time = round(x = time(x = hw_object$fitted), digits = 3), 
                              value_fitted = as.data.frame(hw_object$fitted)$xhat)
  
  actual_values <- data.frame(time = round(x = time(hw_object$x), digits = 3),  
                              Actual = c(hw_object$x))

  graphset <- merge(actual_values,  fitted_values,  by = 'time', all = TRUE)
  
  graphset <- merge(graphset, for_values, all = TRUE, by = 'time')
  
  graphset[is.na(graphset$dev),]$dev <- 0
  
  graphset$Fitted <- c(rep(NA, NROW(graphset) - (NROW(for_values) + NROW(fitted_values))),
                       fitted_values$value_fitted, for_values$value_forecast)

  graphset.melt <- melt(graphset[, c('time', 'Actual', 'Fitted')], id = 'time')
  
  p <- ggplot(graphset.melt,  aes(x=time,  y=value)) + geom_ribbon(data=graphset, aes(x=time, y=Fitted, ymin=Fitted-dev,  ymax=Fitted + dev),  alpha=.2,  fill=error.ribbon) + geom_line(aes(colour=variable), size=line.size) + xlab('Time') + ylab('Value') + scale_colour_hue('')
  
  return(p)
}
